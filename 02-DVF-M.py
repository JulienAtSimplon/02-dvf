import csv, sqlite3
from sqlite3 import Error

""" ---- Déclarations initiales ---- """
MonDossierDeProjet = "/home/julien/Pro/Projets/02-DVF/"
MonFichierBD = MonDossierDeProjet + "DB_Projet_02-DVF.db"
MonDossierDeDataSource = MonDossierDeProjet + "Data/"
MaListeDeFichiersDataSource = [
    "valeursfoncieres-Test.txt",
    ]
""" --- Parametres initiaux --- """
MonDepartementDExtraction = "38"

""" ---- Génération depuis Python du SQL / SQLite ---- """
""" --- Générique --- """
def MaCreationDeConnectionSQLite(UnFichierBD):
    MonConnecteur = None
    try:
        MonConnecteur = sqlite3.connect(MonFichierBD)
    except Error as UneErreurDeConnection:
        print(UneErreurDeConnection)
    return MonConnecteur

def MaCreationDeTable(MonConnecteur, UneTableSQL):
    try:
        UnCurseur = MonConnecteur.cursor()
        UnCurseur.execute(UneTableSQL)
    except Error as UneErreurDeCreationDeTable:
        print(UneErreurDeCreationDeTable)

""" --- Table spécifiques --- """
def MaCreatinDEntete(): # Create_header()
    UnCodeSQL = """ CREATE TABLE IF NOT EXISTS DVF38_Partiel (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                No_disposition TEXT,
                Date_mutation DATE,
                Nature_mutation TEXT,
                Valeur_fonciere FLOAT,
                No_voie TEXT,
                Type_voie TEXT,
                Code_voie TEXT,
                Voie TEXT,
                Code_postal TEXT,
                Commune TEXT,
                Code_departement TEXT,
                Code_commune TEXT,
                Prefixe_de_section TEXT,
                Section TEXT,
                No_plan TEXT,
                Surface_terrain INTEGER
                ); """
    UnConnecteur = MaCreationDeConnectionSQLite(MonFichierBD)
    if UnConnecteur is not None:
        MaCreationDeTable(UnConnecteur, UnCodeSQL)
    else:
        print("Error ! cannot create the database connection.")
""" ---- Fin de : Génération depuis Python du SQL / SQLite ---- """

""" ---- Insertion de Data : Spécifiques ---- """
def ExtractionData():
    UnConnecteur = MaCreationDeConnectionSQLite(MonFichierBD)
    MaCreatinDEntete()
    with open(MonDossierDeDataSource + MaListeDeFichiersDataSource[0], 'r', newline='') as UnAliasDeFichier:
        MonLecteur = csv.DictReader(UnAliasDeFichier, delimiter='|')
        for lignesDeListe in MonLecteur:
                if (lignesDeListe['Code departement']) == MonDepartementDExtraction:
                    #print("Test intermédiaire : " + str(lignesDeListe['Commune']))# Test intermédiaire
                    UneInsertion(UnConnecteur, lignesDeListe) # insertion directe dans la table de la BD
                else:
                    pass

def VerifFloat(Test, Champs, Plan):
    try:
        assert float(Test) > 0
        Champs = Test
        return Champs
    except:
        Champs = None
        print(f"WIP : Erreur dans le data source du plan {Plan} !")
        return Champs

def UneInsertion(Connecteur, LigneListe):
    TempNo_disposition = str(LigneListe['No disposition'])
    TempDate_mutation = (LigneListe['Date mutation'])
    TempNature_mutation = str(LigneListe['Nature mutation'])
    #TempValeur_fonciere : cf Permutations
    TempNo_voie = str(LigneListe['No voie'])
    TempType_voie = str(LigneListe['Type de voie'])
    TempCode_voie = str(LigneListe['Code voie'])
    TempVoie = str(LigneListe['Voie'])
    TempCode_postal = str(LigneListe['Code postal'])
    TempCommune = str(LigneListe['Commune'])
    TempCode_departement = str(LigneListe['Code departement'])
    TempCode_commune = str(LigneListe['Code commune'])
    TempPrefixe_de_section = str(LigneListe['Prefixe de section'])
    TempSection = str(LigneListe['Section'])
    TempNo_plan = str(LigneListe['No plan'])
    TempSurface_terrain = VerifFloat((LigneListe['Surface terrain']).replace(",",".").strip(), LigneListe['Surface terrain'], TempNo_plan)
    #Permutations
    TempValeur_fonciere = VerifFloat((LigneListe['Valeur fonciere']).replace(",",".").strip(), LigneListe['Valeur fonciere'], TempNo_plan)

    TempListe = [TempNo_disposition,
        TempDate_mutation,
        TempNature_mutation,
        TempValeur_fonciere,
        TempNo_voie,
        TempType_voie,
        TempCode_voie,
        TempVoie,
        TempCode_postal,
        TempCommune,
        TempCode_departement,
        TempCode_commune,
        TempPrefixe_de_section,
        TempSection,
        TempNo_plan,
        TempSurface_terrain]
    
    UnCodeSQL = """ INSERT INTO DVF38_Partiel(
                        No_disposition,
                        Date_mutation,
                        Nature_mutation,
                        Valeur_fonciere,
                        No_voie,
                        Type_voie,
                        Code_voie,
                        Voie,
                        Code_postal,
                        Commune,
                        Code_departement,
                        Code_commune,
                        Prefixe_de_section,
                        Section,
                        No_plan,
                        Surface_terrain)
                    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) """
    
    Connecteur.execute(UnCodeSQL, TempListe)
    Connecteur.commit()

#MaCreationDeConnectionSQLite(MonFichierBD)
ExtractionData()
#MaCreatinDEntete()